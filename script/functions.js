// recipe card creation function
export const createCards = (currentRecipe) => {
	//const currentRecipe = recipes[id];
	const ingredients = currentRecipe.ingredients;

	// DOM elements
	let recipesGallery = document.querySelector('.recipesGallery');
	let card = document.createElement('article');
	let cardImg = document.createElement('div');
	let cardRecipe = document.createElement('div');
	let recipeTitle = document.createElement('h2');
	let recipeTime = document.createElement('p');
	let recipeIngredients = document.createElement('ul');
	let recipeDescription = document.createElement('p');

	// Recipeingredients contents
	for(let j = 0; j < ingredients.length; j++){
		let ingredient = document.createElement('li');
		if(ingredients[j].quantity === undefined){
			ingredient.innerHTML = ingredients[j].ingredient;
		}else if(ingredients[j].unit === undefined){
			ingredient.innerHTML = ingredients[j].ingredient + ': ' + ingredients[j].quantity;
		}
		else{
			ingredient.innerHTML = ingredients[j].ingredient + ': ' + ingredients[j].quantity + ' ' + ingredients[j].unit;
		}
		recipeIngredients.appendChild(ingredient);
	}

	// Contents
	recipeTitle.innerHTML = currentRecipe.name;
	recipeTime.innerHTML = '<i class=\'far fa-clock\'></i> ' + currentRecipe.time + ' minutes';
	recipeDescription.innerHTML = currentRecipe.description;

	// AppendChilds
	recipesGallery.appendChild(card);
	card.appendChild(cardImg);
	card.appendChild(cardRecipe);
	cardRecipe.appendChild(recipeTitle);
	cardRecipe.appendChild(recipeTime);
	cardRecipe.appendChild(recipeIngredients);
	cardRecipe.appendChild(recipeDescription);

	// Class
	card.id = currentRecipe.name.toUpperCase();
	card.classList.add('card', 'recipeCard', 'mb-3');
	cardImg.classList.add('card-img-top', 'recipeCard__img');
	cardRecipe.classList.add('card-body', 'recipeCard__body');
	recipeTitle.classList.add('recipeCard__body__title');
	recipeTime.classList.add('card-time', 'recipeCard__body__time');
	recipeIngredients.classList.add('recipeCard__body__list');
	recipeDescription.classList.add('card-text', 'recipeCard__body__description');
};

// return an array of correct recipes ~> cf Array.prototype.filter()
export const arrayFilter = (array, filter) => {
	let newArray = array.filter(recipe => {
		return(
			recipe.name.toUpperCase().includes(filter) ||
            recipe.description.toUpperCase().includes(filter) ||
            recipe.ingredients.some(elt => elt.ingredient.toUpperCase().includes(filter)) ||
            recipe.ustensils.some(elt => elt.toUpperCase().includes(filter)) ||
            recipe.appliance.toUpperCase().includes(filter)
		);
	});

	return newArray;
};

export const displayRightCards = (cardsToDisplay) => {

	const htmlRecipeCards = document.getElementsByClassName('recipeCard');
	const recipeCards = Array.from(htmlRecipeCards);

	recipeCards.forEach(card => {

		if(cardsToDisplay.some(elt => elt.name.toUpperCase() === card.id)){
			document.getElementById(card.id).classList.add('show');
		}

	});

};

export const hideBadCards = (rightCards) => {

	const displayedCards = Array.from(document.getElementsByClassName('show'));

	if(rightCards.length == 0){
		displayedCards.forEach(card => {
			document.getElementById(card.id).classList.remove('show');
		});
		document.querySelector('.error-msg').classList.add('show-error');
	}else{
		document.querySelector('.error-msg').classList.remove('show-error');
		displayedCards.forEach(card => {
			if(rightCards.some(elt => elt.name.toUpperCase() !== card.id)){
				document.getElementById(card.id).classList.remove('show');
			}
		});
	}
};
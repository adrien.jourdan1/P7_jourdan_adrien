// import db
import recipes from './script/recipes.js';
// import intial functions
import { createCards, arrayFilter, displayRightCards, hideBadCards } from './script/functions.js';
import { generateTags, createTags, showAvailableTags, dropMenuManager } from './script/dropdownMenu.js';

// Allow user to open and close dropdownMenus
dropMenuManager();

// Create a card for each recipe
recipes.forEach(elt => createCards(elt)); //4ms
displayRightCards(recipes); //1ms

// Generate all filters in dropdown menu from recipes list
createTags(generateTags(recipes)); //6ms


// SEARCH ALGO

// Search Bar Event
const searchBar = document.getElementById('search-bar');
const searchingRegex = /^.{3,}/g;
let filteredRecipesByString = [];
let filteredRecipesByTags = [];
let filteredRecipes = [];
const filters = [];

searchBar.addEventListener('keyup', (e) => {

	if(searchingRegex.exec(searchBar.value) != null){ //6ms for "coco"

		const searchString = e.target.value.toUpperCase();

		// Create a new array from the search string and recipes list
		filteredRecipesByString = arrayFilter(recipes, searchString);

		// Create a new array from filterRecipeByTags AND filteredRecipesByString
		if(filteredRecipesByTags.length > 0){
			filteredRecipes = filteredRecipesByTags.filter(elt => {
				return filteredRecipesByString.includes(elt);
			});
		}else{
			filteredRecipes = filteredRecipesByString;
		}

		// Then display recipes and update available tags in dropdown menus
		hideBadCards(filteredRecipes);
		displayRightCards(filteredRecipes);
		showAvailableTags(filteredRecipes, filters);

	}
	else if(searchBar.value == ''){

		filteredRecipesByString = [];

		// Create a new array from filterRecipeByTags AND filteredRecipesByString
		if(filteredRecipesByTags.length > 0){
			filteredRecipes = filteredRecipesByTags;
		}else{
			filteredRecipes = recipes;
		}

		// Then display recipes and update available tags in dropdown menus
		hideBadCards(filteredRecipes);
		displayRightCards(filteredRecipes);
		showAvailableTags(filteredRecipes, filters);
	}

});


// DropdownMenu Events
const htmlDropdownItems = document.getElementsByClassName('dropdown-item');
const dropdownItems = Array.from(htmlDropdownItems); // transform html collection into an array for event use
dropdownItems.forEach(elt => elt.addEventListener('click', (e) => {

	let filter = {
		name: e.target.innerHTML.toUpperCase(),
	};

	// looking for parent Id and add a prop to filter object
	if(e.target.parentNode.id == 'ingredients-dropdown-menu'){

		Object.defineProperty(filter, 'type', {value: 'ingredient'});

	}else if(e.target.parentNode.id == 'appliances-dropdown-menu'){

		Object.defineProperty(filter, 'type', {value: 'appliance'});

	}else if(e.target.parentNode.id == 'utensils-dropdown-menu'){

		Object.defineProperty(filter, 'type', {value: 'utensil'});
	}

	// Display the new filter in html + manage Remove tag Event
	displayTags(filter); // this function is at the end of this file
    
	// Add new filter into the filters' list
	filters.push(filter);
    
	// Create a new array from the new filter and recipes list
	if(filteredRecipesByTags.length > 0){
		filteredRecipesByTags = arrayFilter(filteredRecipesByTags, filter.name);
	}else{
		filteredRecipesByTags = arrayFilter(recipes, filter.name);
	}

	// Create a new array from filterRecipeByTags AND filteredRecipesByString
	if(filteredRecipesByString.length > 0){
		filteredRecipes = filteredRecipesByTags.filter(elt => {
			return filteredRecipesByString.includes(elt);
		});
	}else{
		filteredRecipes = filteredRecipesByTags;
	}

	// Then display recipes and update available tags in dropdown menus
	hideBadCards(filteredRecipes);
	displayRightCards(filteredRecipes);
	showAvailableTags(filteredRecipes, filters);
}));



// DropMenu Search Bar Event
const dropSearchBars = Array.from(document.getElementsByClassName('drop__menu__search'));

dropSearchBars.forEach((dropSearchBar, index) => dropSearchBar.addEventListener('input', () => {
	let selectedTagsClass;
	const allShownTags = Array.from(document.getElementsByClassName('show-tag'));

	if(index == 0){
		selectedTagsClass = 'ingredient-tag'; 
	}else if(index == 1){ 
		selectedTagsClass = 'appliance-tag'; 
	}else if(index == 2){ 
		selectedTagsClass = 'utensil-tag'; 
	}
    
	// create a new Array that contains only shown tags in the current open menu
	const selectedShowTags = allShownTags.filter(shownTag => shownTag.className.includes(selectedTagsClass));

	if(searchingRegex.exec(dropSearchBar.value) != null){

		const dropSearchString = dropSearchBar.value.toUpperCase();

		// create a new Array that contains only show tags that correspond to user's search
		const searchedTags = selectedShowTags.filter(shownTag => shownTag.textContent.toLocaleUpperCase().includes(dropSearchString));

		selectedShowTags.forEach(tag => {
			if(searchedTags.some(elt => elt === tag)){
				tag.classList.remove('hide-tag');
				tag.classList.add('show-tag');
			}else{
				tag.classList.remove('show-tag');
				tag.classList.add('hide-tag');
			}
		});

	}else if (dropSearchBar.value == '' && filteredRecipes.length < 1){
		showAvailableTags(recipes, filters);
	}else if(dropSearchBar.value == '' && filteredRecipes.length > 0){
		showAvailableTags(filteredRecipes, filters);
	}

    
}));


// display tag under Search bar + Remove tag Event
const displayTags = (filter) => {

	const htmlTags = document.getElementById('currentTags');
	const tagFilter = document.createElement('button');

	tagFilter.classList.add('btn', 'mr-3', 'mb-3');

	if(filter.type == 'ingredient'){
		tagFilter.classList.add('btn--blue');
	}
	if(filter.type == 'appliance'){
		tagFilter.classList.add('btn--green');
	} 
	if(filter.type == 'utensil'){
		tagFilter.classList.add('btn--red');
	} 

	tagFilter.innerHTML = filter.name.toLowerCase() + '<i class="close-icon far fa-times-circle"></i>';

	htmlTags.appendChild(tagFilter);
    
	// remove tag event
	tagFilter.addEventListener('click', () => {

		htmlTags.removeChild(tagFilter);
		filteredRecipesByTags = recipes;

		// remove the tag from filters[]
		// have to duplicate the "forEach" because of edge case with only two filters in filters[]
		filters.forEach((elt, index) => {
			if(elt === filter){
				filters.splice(index, 1);
			}
		});

		// then recreate filteredRecipesByTags[] with all filters in filters[]
		filters.forEach(elt => {
			filteredRecipesByTags = arrayFilter(filteredRecipesByTags, elt.name);
		});
        
		// Create a new array from filterRecipeByTags AND filteredRecipesByString
		if(filteredRecipesByString.length > 0){
			filteredRecipes = filteredRecipesByTags.filter(elt => {
				return filteredRecipesByString.includes(elt);
			});
		}else{
			filteredRecipes = filteredRecipesByTags;
		}

		// Then display recipes and update available tags in dropdown menus
		hideBadCards(filteredRecipes);
		displayRightCards(filteredRecipes);
		showAvailableTags(filteredRecipes, filters);
	});
};
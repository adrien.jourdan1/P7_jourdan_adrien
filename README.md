![Logo Les petits plats](https://user.oc-static.com/upload/2020/08/14/15973932905401_logo%20%281%29.png)

# Welcome to Les Petits Plats

Les Petits Plats is the seventh project of the Openclassrooms Front-End developer path !



# Scenario

You are a freelance and you have just been commissioned by the company "Les Petits Plats" as a Front-end Developer for a 3-month mission. 

After having edited cookbooks for several years, the company decided to embark on a new project: to create its own cooking recipe site like Marmiton or 750g.

Sandra, project manager, is in charge of the digitalization of the company with the creation of this website. For now, she only works with freelancers like you, before creating an in-house team to manage this project. To make sure you have everything you need available, she sends you an email: 


> From: Sandra<br>To: me<br>Subject: Research algo details

>Hello,<br><br>I am delighted to have you on the team for this new phase of the project.<br><br>As you know, there are many sites offering cooking recipes and the team thought that one of the things that can make the difference on our site is the fluidity of the search engine. As the Back-end team is not yet formed, we only have a [JavaScript file containing a JSON array of 50 recipes](https://github.com/OpenClassrooms-Student-Center/P11-front-end-search-engine).<br><br>Your first mission will therefore be to implement the search functionality. Here you can find [the description of the search use case](https://s3-eu-west-1.amazonaws.com/course.oc-static.com/projects/Front-End+V2/P6+Algorithms/Cas+d%E2%80%99utilisation+%2303+Filtrer+les+recettes+dans+l%E2%80%99interface+utilisateur.pdf). It is this document that will serve as a reference for all the development of this feature. On top of that, here's [the mockup of the page on Figma](https://www.figma.com/file/xqeE1ZKlHUWi2Efo8r73NK), make sure you stick to the design to the letter.<br><br>What we want above all is something powerful because our users want a fast, almost instantaneous search! Your work will be transmitted to the Back-end in a second step to be adapted by them. This is why you will need to send them a document explaining your work well. I'll let you see how to proceed in detail directly with John the Baptist.<br><br>Sandra




At the end of the morning, you receive a Slack notification from Jean-Baptiste, your Lead Developer: 

>JB : Hi ! As you can see, research is a very important feature for the team and we are counting on you to develop it optimally. <br><br>In our team, for any important algorithm that we develop, we tend to make two different versions so that we can compare their performance and choose the best one. So you will have to do the same! For that you will need to create a comparison document called a "feature investigation sheet". We recently did this for the "login / register" feature, and [here is the result](https://s3-eu-west-1.amazonaws.com/course.oc-static.com/projects/Front-End+V2/P6+Algorithms/Fiche+d%E2%80%99investigation+fonctionnalite%CC%81.pdf). So reuse the same document template directly.

>You : Very well thank you. So, how do you advise me to proceed?

>JB : So my advice is to start by implementing your interface so that you get rid of it. Like the rest of the site, you can use Bootstrap 4 if you want, but no other library. Make sure you write code that successfully passes the W3C validator. <br><br>Then, on the algorithms side, you can proceed in 3 steps. <br><br>First, plan the 2 versions of the feature you want to test. To do this, start filling out the feature investigation document as much as you can to properly describe the two algorithms you want to compare.<br><br>These two algorithms should focus only on the main search field.<br><br>Do not forget to draw a diagram for each of the proposals so that we understand the sequence of steps of each of the algorithms, this will be especially useful for the Back-end team. You can use the diagrams in the Connection / Registration feature investigation sheet, but use whatever formatting you want.

>You : Ok and then I implement them?

>JB : Exactly, second step: you implement them both. To do this, use 2 different branches on Git so that we keep the separate code for each. For your implementation, all the technical info is on the use case document that Sandra sent you. For searches by tag, you can use one and the same version of the search for the 2 branches.

>You : And how do I choose the best algorithm?

>JB : This is your third and final step. To choose the best algorithm, you need to test their performance. For that, you can use any performance comparison tool you want, personally I use Jsben.ch for this kind of analysis. It will give you the number of operations per second performed by each script, so you can see at a glance which script is performing the best. You can only test the main search (no need to use filters). Then add the results to the feature investigation sheet that you have written. Don't forget to end the document with the algorithm recommendation to keep following your analysis and tests.

>You : Perfect, thank you for your advice JB. I'm going for it !

# Deliverables 


- A feature investigation sheet on the search algorithm (PDF format). You will integrate the choice of the final algorithm in comparison to the other research algorithm developed.

- A link to the GitHub repo of the project including the two branches with the two different solutions for research. 

# Contributing

>Adrien JOURDAN ->  [check my gitlab](https://gitlab.com/adrien.jourdan1)

>translated from French by **Google** ... ^^ *because I was lazy*